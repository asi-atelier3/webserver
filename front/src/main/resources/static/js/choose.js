const API = "cards"

let cardList = [];

function getCards() {
    const xhr = new XMLHttpRequest();
    let url = getURL(API) + "?uid=" + userID
    xhr.open("GET", url, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            // Changing string data into JSON Object
            obj = JSON.parse(this.responseText);
            cardList = obj;
            displayCards();
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();
}

function chooseCard(cardID) {
    const xhr = new XMLHttpRequest();
    roomID = localStorage.getItem('roomID')
    xhr.open("PUT", getURL("rooms") + roomID + "?cid=" + cardID + "&uid=" + userID, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            console.log(this.responseText);
            if(this.responseText) window.location.href = URLPREFIX + ":5500/fight.html";
        } else {
            console.log("File not found");
        }
    }
    xhr.send();
}

function displayCards() {
    let template = document.querySelector("#row");

    if (!cardList) {
        document.getElementById("error_card").hidden = false;
    } else {

        if (cardList.length == 0) {
            document.getElementById("error_card").hidden = false;
            return;
        }

        for (const card of cardList) {
            let clone = document.importNode(template.content, true);

            newContent = clone.firstElementChild.innerHTML
                .replace(/{{family}}/g, card.family)
                .replace(/{{image_url}}/g, card.image_url)
                .replace(/{{name}}/g, card.name)
                .replace(/{{description}}/g, card.description)
                .replace(/{{hp}}/g, card.hp)
                .replace(/{{energy}}/g, card.energy)
                .replace(/{{attack}}/g, card.attack)
                .replace(/{{defense}}/g, card.defense)

            clone.firstElementChild.innerHTML = newContent;

            clone.firstElementChild.lastElementChild.onclick = function () { chooseCard(card.id) };
            clone.firstElementChild.onclick = function () {
                $(function () {
                    $("#card").html($("#card").load("./part/card-full.html", function(){
                        $("#card").html($("#card").html()
                        .replace(/{{family}}/g, card.family)
                        .replace(/{{image_url}}/g, card.image_url)
                        .replace(/{{name}}/g, card.name)
                        .replace(/{{description}}/g, card.description)
                        .replace(/{{hp}}/g, card.hp)
                        .replace(/{{energy}}/g, card.energy)
                        .replace(/{{attack}}/g, card.attack)
                        .replace(/{{defense}}/g, card.defense)
                        .replace(/{{price}}/g, card.price))
                        // add listener for buyCard button
                        $("#card .ui.bottom.attached.button").on("click", function(){ chooseCard(card.id) });
                    })
                    )
                });
            };
            let cardContainer = document.querySelector("#tableContent");
            cardContainer.appendChild(clone);
        }
    }
}

getCards();
