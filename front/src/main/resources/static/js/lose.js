const API = "rooms"

function getRoom() {
	const xhr = new XMLHttpRequest();
    let url = getURL(API) + "/" + localStorage.getItem('roomID');
    xhr.open("GET", url, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            obj = JSON.parse(this.responseText);
			replaceBet(obj.bet);
        } else {
            console.log("File not found");
        }
    }
    // At last send the request
    xhr.send();
}

function replaceBet(bet) {
	$("#bet").html($("#bet").html().replace(/{{bet}}/g, bet))
}

function replaceRoomID() {
	$("#roomID").html($("#roomID").html().replace(/{{roomID}}/g, localStorage.getItem('roomID')))
}

replaceRoomID();
getRoom();

