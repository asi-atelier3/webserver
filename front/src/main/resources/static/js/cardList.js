let user = undefined;

let cardList = [];

//TODO
function getUser() {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8080/api/users", true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function() {
        
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {

            // Changing string data into JSON Object
            let obj = JSON.parse(this.responseText);
            console.log(this.obj);
            user = this.obj;
            
            displayUser();
            
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();
}

function displayUser() {
    document.getElementById("user_profile").hidden = false;
}



function getCards() {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:8080/api/cards", true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function() {
        
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {

            // Changing string data into JSON Object
            obj = JSON.parse(this.responseText);
            
            cardList = this.obj;
            
            
            cardList = [
        {
            family_name:"DC Comic",
            image_url:"http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
            name:"SUPERMAN",
            description: "The origin story of Superman relates that he was born Kal-El on the planet Krypton, before being rocketed to Earth as an infant by his scientist father Jor-El, moments before Krypton's destruction. Discovered and adopted by a farm couple from Kansas, the child is raised as Clark Kent and imbued with a strong moral compass. Early in his childhood, he displays various superhuman abilities, which, upon reaching maturity, he resolves to use for the benefit of humanity through a 'Superman' identity.",
            hp: 500,
            energy:100,
            attack:50,
            defense: 50,
            price:200
        },
        {
            family_name:"DC Comic",
            image_url:"https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg",
            name:"BATMAN",
            description: "Bruce Wayne, alias Batman, est un héros de fiction appartenant à l'univers de DC Comics. Créé par le dessinateur Bob Kane et le scénariste Bill Finger, il apparaît pour la première fois dans le comic book Detective Comics no 27 (date de couverture : mai 1939 mais la date réelle de parution est le 30 mars 1939) sous le nom de The Bat-Man. Bien que ce soit le succès de Superman qui ait amené sa création, il se détache de ce modèle puisqu'il n'a aucun pouvoir surhumain. Batman n'est qu'un simple humain qui a décidé de lutter contre le crime après avoir vu ses parents se faire abattre par un voleur dans une ruelle de Gotham City, la ville où se déroulent la plupart de ses aventures. Malgré sa réputation de héros solitaire, il sait s'entourer d'alliés, comme Robin, son majordome Alfred Pennyworth ou encore le commissaire de police James Gordon. ",
            hp: 50,
            energy:80,
            attack:170,
            defense: 80,
            price:100
        },
        {
            family_name:"Marvel",
            image_url:"https://static.hitek.fr/img/actualite/2017/06/27/i_deadpool-2.jpg",
            name:"DEAD POOL",
            description: "Le convoi d'Ajax est attaqué par Deadpool. Il commence par massacrer les gardes à l'intérieur d'une voiture, avant de s'en prendre au reste du convoi. Après une longue escarmouche, où il est contraint de n'utiliser que les douze balles qu'il lui reste, Deadpool capture Ajax (dont le véritable nom est Francis, ce que Deadpool ne cesse de lui rappeler). Après l'intervention de Colossus et Negasonic venus empêcher Deadpool de causer plus de dégâts et le rallier à la cause des X-Men, Ajax parvient à s'échapper en retirant le sabre de son épaule. Il apprend par la même occasion la véritable identité de Deadpool : Wade Wilson.",
            hp: 999999,
            energy:100,
            attack:15,
            defense: 15,
            price:250
        },

    ]
            displayCards();
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();

}

//TODO
function buyCard(cardID){
 	const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/api/cards", true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    xhr.onload = function() {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
			//TODO
        } else {
            console.log("File not found");
        }
    }
    xhr.send();


}


function displayCards() {
    let template = document.querySelector("#row");
    
    if(!cardList){
    	console.log("Cards not found")
    	 document.getElementById("error_card").hidden = false;
    }else{
    
     for (const card of cardList) {
        let clone = document.importNode(template.content, true);

        newContent = clone.firstElementChild.innerHTML
            .replace(/{{family_src}}/g, card.family_src)
            .replace(/{{family_name}}/g, card.family_name)
            .replace(/{{image_url}}/g, card.image_url)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defense)
            .replace(/{{price}}/g, card.price);
            
        clone.firstElementChild.innerHTML = newContent;
        
        clone.firstElementChild.lastElementChild.onclick = function() {buyCard(card.id)};
        
		console.log();
        let cardContainer = document.querySelector("#tableContent");
        cardContainer.appendChild(clone);
    	}
    }   
}

getCards();
