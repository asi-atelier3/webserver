// Defining constants
const URLPREFIX = "http://localhost"
const PORTS = {
    users : "",
    cards : "",
    images : "",
    rooms : "",
    fights : ""
}
const URLSUFFIX = "/api"
const APIS = {
    users : "users/",
    cards : "cards/",
    images : "images/",
    rooms : "rooms/",
    fights : "fights/"
}
function getURL(api){
    return URLPREFIX + ":" + PORTS[api] + URLSUFFIX + "/" + APIS[api];
}

let user = undefined;
if(localStorage.getItem("userID") == null) localStorage.setItem("userID", "1");
let userID = localStorage.getItem("userID");


 $(function(){
 	$("#userdiv").load("./part/user.html");
 	getUser();
 });

 
function getUser() {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", getURL("users")+userID, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function() {
        if (this.status === 200) {
        
            let obj = JSON.parse(this.responseText);
            user = obj;
            
            displayUser();
            
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();
}

function displayUser() {
	document.getElementById("userNameId").innerText = user.username;
	document.getElementById("userMoney").innerText = user.money;
    document.getElementById("user_profile").hidden = false;
}
