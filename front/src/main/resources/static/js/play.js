const API = "rooms"
let roomList = [];

function getRooms() {
    const xhr = new XMLHttpRequest();
    let url = getURL(API);
    xhr.open("GET", url, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    // When response is ready
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            // Changing string data into JSON Object
            obj = JSON.parse(this.responseText);
            roomList = obj;
            displayRooms();
        } else {
            console.log("File not found");
        }
    }

    // At last send the request
    xhr.send();
}
// PUT /rooms/:id?uid=uid&cid=cid
//TODO
function joinRoom(roomID) {
    localStorage.setItem('roomID', roomID);
    console.log(localStorage.getItem('roomID'))
    window.location.href = URLPREFIX + ":5500/chooseCard.html";
}

function displayRooms() {
    let template = document.querySelector("#row");

    if (!roomList) {
        document.getElementById("error_room").hidden = false;
    } else {

        if (roomList.length == 0) {
            document.getElementById("error_room").hidden = false;
            return;
        }

        for (const room of roomList) {
            let clone = document.importNode(template.content, true);

            newContent = clone.firstElementChild.innerHTML
            .replace(/{{creator}}/g, room.creatorId)
            .replace(/{{bet}}/g, room.bet);
            
            // get User data via the api
            const xhr = new XMLHttpRequest();
            xhr.open("GET", getURL("users") + room.creatorId, true);
            xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
            xhr.onload = function () {
                if(this.status === 401){
                    window.location.href = '/login.html';
                }
                if (this.status === 200) {
                    // Changing string data into JSON Object
                    obj = JSON.parse(this.responseText);
                    
                    let roomContainer = document.querySelector("#creatorUsername");
                    roomContainer.innerHTML = obj.username;
                } else {
                    console.log("File not found");
                }
            }
            xhr.send();

            clone.firstElementChild.innerHTML = newContent;

            clone.firstElementChild.lastElementChild.onclick = function () { joinRoom(room.id) };

            let roomContainer = document.querySelector("#tableContent");
            roomContainer.appendChild(clone);
        }
    }
}
function createRoom(){
    window.location.href = URLPREFIX + ":5500/createRoom.html";
}

getRooms();
