let cardCreator
let cardAdversary

function displayCard(cardName, card) {
    $("#" + cardName).html($("#" + cardName).load("./part/card-short.html", function () {
        // get room from the localstorage's roomID
        const roomID = localStorage.getItem('roomID');
        const xhr = new XMLHttpRequest();
        xhr.open("GET", getURL("rooms") + roomID, true);
        xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
        xhr.onload = function () {
            if(this.status === 401){
                window.location.href = '/login.html';
            }
            if (this.status === 200) {
                if (this.responseText) {
                    const room = JSON.parse(this.responseText);
                }
            }
        }
        $("#" + cardName).html($("#" + cardName).html()
            .replace(/{{family}}/g, card.family)
            .replace(/{{image_url}}/g, card.image_url)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defense)
            .replace(/{{price}}/g, card.price))
        // add listener for buyCard button
        $("#card .ui.bottom.attached.button").on("click", function () { chooseCard(card.id) });
    }))
}
function addCards() {
    // get room from the localstorage's roomID
    const roomID = localStorage.getItem('roomID');
    const xhr = new XMLHttpRequest();
    xhr.open("GET", getURL("rooms") + roomID, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            if (this.responseText) {
                const room = JSON.parse(this.responseText);
                // set play
                if (room.creatorId == userID) localStorage.setItem("player", 1);
                else localStorage.setItem("player", 2);
                // get user card from API with room.creatorId and displayCard
                const xhrc = new XMLHttpRequest();
                xhrc.open("GET", getURL("cards") + room.creatorCardId, true);
                xhrc.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
                xhrc.onload = function () {
                    if(this.status === 401){
                        window.location.href = '/login.html';
                    }
                    if (this.status === 200) {
                        if (this.responseText) {
                            cardCreator = JSON.parse(this.responseText);
                            displayCard("cardCreator", cardCreator);
                        }
                        // get user card from API with room.adversaryId and displayCard
                        const xhra = new XMLHttpRequest();
                        xhra.open("GET", getURL("cards") + room.adversaryCardId, true);
                        xhra.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
                        xhra.onload = function () {
                            if(this.status === 401){
                                window.location.href = '/login.html';
                            }
                            if (this.status === 200) {
                                cardAdversary = JSON.parse(this.responseText);
                                displayCard("cardAdversary", cardAdversary);
                                addFight();
                            }
                        }
                        xhra.send();
                    }
                }
                xhrc.send();
            }
        }
        else {
            console.log("File not found");
        }
    }
    xhr.send();
}

function attack() {
    // manage card attack from fights API and display the new cards
    let roomID = localStorage.getItem("roomID")
    let player = localStorage.getItem('player')
    const xhr = new XMLHttpRequest();
    xhr.open("PUT", getURL("fights") + roomID + "?player=" + player, true);
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            if (this.responseText) {
                const fight = JSON.parse(this.responseText);
                // display new cards
                cardCreator.hp = fight.p1Hp;
                cardCreator.energy = fight.p1Energy;
                cardAdversary.hp = fight.p2Hp;
                cardAdversary.energy = fight.p2Energy;
                displayCard("cardCreator", cardCreator);
                displayCard("cardAdversary", cardAdversary);
            }
        }
    }
    xhr.send(JSON.stringify([cardCreator, cardAdversary]));
}
function addFight() {
    // add fight to fights API
    const xhr = new XMLHttpRequest();
    xhr.open("POST", getURL("fights"), true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
    xhr.onload = function () {
        if(this.status === 401){
            window.location.href = '/login.html';
        }
        if (this.status === 200) {
            if (this.responseText) {
                const fight = JSON.parse(this.responseText);
            }
        }
    }
    xhr.send(JSON.stringify({
        "id": localStorage.getItem('roomID'),
        "p1Energy": cardCreator.energy,
        "p1Hp": cardCreator.hp,
        "p2Energy": cardAdversary.energy,
        "p2Hp": cardAdversary.hp,
    }));
}
function fight() {
    player = localStorage.getItem('player')
    addCards();
    // attack every 2 seconds until a card's hp is null
    var interval = setInterval(function () {
        if (cardCreator.hp > 0 && cardAdversary.hp > 0) {
            attack();
        }
        else {
            // redirect to draw.html if both cards's hp are to 0
            if (cardCreator.hp == 0 && cardAdversary.hp == 0) {
                finish("draw");
                clearInterval(interval);
            }
            // log win loose or draw depending on the player
            else if (cardCreator.hp == 0) {
                if (player == 1) finish("loose")
                else finish("win")
                clearInterval(interval);
            }
            else {
                if (player == 1) finish("win")
                else finish("loose")
                clearInterval(interval);
            }
            clearInterval(interval);
        }
    }, 2000);
}
function finish(victory) {
    console.log(victory);
    if (victory == "draw") {
        window.location.href = "draw.html";
    }
    else {
        // get user
        const xhr = new XMLHttpRequest();
        xhr.open("GET", getURL("users") + userID, true);
        xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
        xhr.onload = function () {
            if(this.status === 401){
                window.location.href = '/login.html';
            }
            if (this.status === 200) {
                if (this.responseText) {
                    const user = JSON.parse(this.responseText);
                    console.log(user)
                    // update user's money
                    const xhr = new XMLHttpRequest();
                    xhr.open("PUT", getURL("users") + userID, true);
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader('Authorization','Bearer ' + localStorage.getItem("token"));
                    xhr.onload = function () {
                        if(this.status === 401){
                            window.location.href = '/login.html';
                        }
                        if (this.status === 200) {
                            if (this.responseText) {
                                const user = JSON.parse(this.responseText);
                                localStorage.setItem("user", user);
                            }
                        }
                    }
                    if (victory == "win") {
                        xhr.send(JSON.stringify({
                            "id": userID,
                            "money": user.money + parseInt(localStorage.getItem("bet")),
                            "mail":user.mail,
                            "username":user.username
                        }));
                        // window.location.href = "win.html";
                    }
                    else {
                        xhr.send(JSON.stringify({
                            "id": userID,
                            "money": user.money - parseInt(localStorage.getItem("bet")),
                            "mail":user.mail,
                            "username":user.username
                        }));
                        // window.location.href = "lose.html";
                    }
                }
            }
        }
        xhr.send();
    }
}
fight();
